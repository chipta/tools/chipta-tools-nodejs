var fs            = require('fs')
var http          = require('https')
var fileName      = process.argv[process.argv.indexOf('-file') + 1]
var username      = process.argv[process.argv.indexOf('-user') + 1]
var password      = process.argv[process.argv.indexOf('-pass') + 1]
var data          = fs.readFileSync(fileName, 'utf8')
var parsedData    = JSON.parse(data)
var path          = `/rest/`
var authObj       = {username:username, password:password}
var options       = {
  host   : 'www.chipta.com',
  method : 'PATCH',
  headers: {
    'Content-Type': 'application/json',
    'Accept'      : 'application/json'
  }
}

function cloneObject (o) {return JSON.parse(JSON.stringify(o))}

function handleResponse (resolve, response) {
  var data = ''
  response.on('data', chunk => {data += chunk})

  response.on('end', () => {
    console.log('product:', data)

    if (response.statusCode === 200) resolve()
    else console.log('status ' + response.statusCode)
  })
}

function makeGuestProductPath (eventId, guestProductId) {
  return `${path}events/${eventId}/guestproducts/${guestProductId}/`
}

function confirm (eventId, guestProductId, status) {
  var guestProductOpions  = cloneObject(options)
  guestProductOpions.path = makeGuestProductPath(eventId, guestProductId)

  return new Promise(
    (resolve, reject) => {
      http.request(
        guestProductOpions,
        response => {handleResponse(resolve, response)}
      ).end(JSON.stringify({status}))
    }
  )
}

function doConfirmMessage (message) {
  return confirm(
    message.eventId,
    message.id,
    message.status
  )
}

function confirmMessage (message) {
  return doConfirmMessage(message)
  .catch(e => {console.log(message, e)})
}

function repeat (func, list) {
  let promise = Promise.resolve()
  let times = list.length

  while (times-- > 0) {
    const item = list[times]
    promise = promise.then(() => func(item))
  }
  return promise
}

function authCallback (response) {
  console.log('authorisation request status:', response.statusCode)
  var tokenObjectJson = ''
  response.on('data', chunk => {tokenObjectJson += chunk})

  response.on('end', () => {
    var authToken = JSON.parse(tokenObjectJson).token
    options.headers.Authorization = `Token ${authToken}`

    repeat(confirmMessage, parsedData)
  })
}

var authOptions    = cloneObject(options)
authOptions.path   = `${path}auth-token/`
authOptions.method = 'POST'

console.log(authOptions)

http.request(authOptions, authCallback).end(JSON.stringify(authObj))
