# Chipta tools

Tools die vrij te gebruiken zijn door zowel medewerkers als klanten

## assign-guest-products.js

You can use assign-guest-products.js to add guests with tickets to an event. This script assumes a linux environment with nodejs installed. Run `npm i` before using it to install the csv package.

The following command line arguments are required:

- `-user` username, typically an e-mail address.
- `-pass` password
- `-file` filename of csv input file
- `-event` eventId
- `-product` id of product to add to the guests specified in the csv file.

The csv file should be formatted as follows:

| first | last | email | amount |
|---|---|---|---|
| first name | last name | e-mail address | amount of products |

example:

```bash
node assign-guest-products.js -user name@email.com -pass Passw0rd -file './csv data file.csv' -event 1234 -product 56789
```

## manually-send-message-queue.js

If a scan-app was unable to send their message queue, this script can send the messages once they are recuperated from the mobile device.

The following command line arguments are required:

- `-user` username, typically an e-mail address.
- `-pass` password
- `-file` filename of csv input file

A message queue looks like this (in a *.json file):

```json
[
  {
    "status" : "used",
    "id"     : 3999562,
    "eventId": 4576
  },
  {
    "status" : "used",
    "id"     : 3997507,
    "eventId": 4576
  }
]
```

to get the message queue from the device:

- `adb backup -noapk com.chipta`, geen password invoeren op device
- `dd if=backup.ab bs=1 skip=24 | python -c "import zlib,sys;sys.stdout.write(zlib.decompress(sys.stdin.read()))" | tar -xvf -`
- `sqlite3 apps/com.chipta/db/RKStorage`
- `.output ./backup.sql`
- `.dump catalystLocalStorage`
- `.quit`
- extract contents of `messageQueue` to `messageQueue.json`

Then finally:

```bash
node manually-send-message-queue.js -user name@email.com -pass Passw0rd -file './messageQueue.json'
```
