const error = "error"
const never = "never"
const always = "always"
const off = "off"
const warn = "warn"

module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "extends": ["eslint:recommended", "standard"],
    "parserOptions": {
        "ecmaVersion": 2018
    },
    "rules": {
        "no-console": "off",
        "key-spacing": [warn, {
          "singleLine": {
            "beforeColon": false,
            "afterColon" : false
          },
          "align"     : {
            "beforeColon": false,
            "afterColon" : true,
            "on"         : "colon"
          }
        }],
        "space-before-function-paren": [error, always],
        "object-curly-spacing"       : [error, never],
        "block-spacing"              : [error, never],
        "curly"                      : off, // [error, "multi-or-nest"],
        "spaced-comment"             : off,
        "space-in-parens"            : off,
        "indent"                     : off,
        "no-multi-spaces"            : off,
        "no-multiple-empty-lines"    : off,
        "padded-blocks"              : off,
        "no-inline-comments": off,
        "line-comment-position": off
    }
};
