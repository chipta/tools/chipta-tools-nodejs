var parse         = require('csv-parse/lib/sync')
var fs            = require('fs')
var http          = require('https')
var fileName      = process.argv[process.argv.indexOf('-file') + 1]
var username      = process.argv[process.argv.indexOf('-user') + 1]
var password      = process.argv[process.argv.indexOf('-pass') + 1]
var event         = process.argv[process.argv.indexOf('-event') + 1]
var productId     = process.argv[process.argv.indexOf('-product') + 1]
var data          = fs.readFileSync(fileName, 'utf8')
var parsed        = parse(data, {columns:true})
var host          = 'www.chipta.com'
var port          = ''
var path          = `/rest/events/${event}/`
var authObj       = {username:username, password:password}
var options       = {
  host    : host,
  path    : path,
  port    : port,
  protocol: 'http:',
  method  : 'POST',
  headers : {
    'Content-Type': 'application/json',
    'Accept'      : 'application/json'
  }
}

function cloneObject (o) {return JSON.parse(JSON.stringify(o))}

console.log('contacting url: ', host + (port ? ':' + port : '') + path)


function handleGuestResponse (resolve, response, amount) {
  var guest = ''
  response.on('data', chunk => {guest += chunk})

  response.on('end', () => {

    console.log('create guest status code:', response.statusCode)

    try {
      guest = JSON.parse(guest)
    } catch (e) {
      console.log(guest)
    }

    if (response.statusCode === 201) {
      console.log(`guest ${guest.user.first_name} ${guest.user.last_name} succesfully created with id: ${guest.id}`)
      guest.amount = amount
      resolve(guest)
    } else throw new Error('guest status ' + response.statusCode)
  })
}

function createGuest (resolve, guest) {
  var userData = {
    first_name: guest.first,
    last_name : guest.last,
    email     : guest.email
  }

  var guestData = {
    user  : userData,
    email : guest.email,
    event : event,
    status: 'invited'
  }

  var guestOpions  = cloneObject(options)
  guestOpions.path += 'guests/'

  http.request(guestOpions, response => {
    handleGuestResponse(resolve, response, guest.amount)
  }).end(JSON.stringify(guestData))
}

function handleProductResponse (resolve, response, guest) {
  var product = ''
  response.on('data', chunk => {product += chunk})

  response.on('end', () => {
    try {
      product = JSON.parse(product)
    } catch (e) {
      console.log('product:', product, 'error:', e)
    }

    if (response.statusCode === 201) {
      console.log(`product ${product.id} succesfully created for guest ${product.guest}`)
      resolve(guest)
    } else throw new Error('product status ' + response.statusCode)
  })
}

function doCreateTickets (guest, eventProduct) {
  var productData = {
    guest        : guest.id,
    event_product: eventProduct,
    status       : 'available'
  }

  var productOptions = cloneObject(options)
  productOptions.path += 'guestproducts/'

  return new Promise((resolve, reject) => {
    http.request(productOptions, response => {
      handleProductResponse(resolve, response, guest)
    }).end(JSON.stringify(productData))
  })
}

function createTickets (guest, eventProduct) {
  function repeat (func, times) {
    var promise = Promise.resolve()
    while (times-- > 0) promise = promise.then(func)
    return promise
  }

  return repeat(() => doCreateTickets(guest, eventProduct), guest.amount)
}


function chainPromises (promiseChain, guest) {
  return (
    promiseChain
    .then(() => new Promise(resolve => createGuest(resolve, guest))
    .then(guest => createTickets(guest, productId)))
  )
}

function createGuestsAndTickets () {
  parsed.reduce(chainPromises, Promise.resolve())
  .then(() => {console.log('done')}, er => {console.log(er)})
}


function authCallback (response) {
  console.log('authorisation request status:', response.statusCode)
  var tokenObjectJson = ''
  response.on('data', chunk => {tokenObjectJson += chunk})

  response.on('end', () => {
    var authToken = JSON.parse(tokenObjectJson).token
    options.headers.Authorization = `Token ${authToken}`

    createGuestsAndTickets()
  })
}


var authOptions    = cloneObject(options)
authOptions.path   = '/rest/auth-token/'
authOptions.method = 'POST'

http.request(authOptions, authCallback).end(JSON.stringify(authObj))
